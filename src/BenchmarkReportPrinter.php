<?php

namespace XsolveBenchmark;

interface BenchmarkReportPrinter
{
  /**
   * Print benchmark report
   *
   * @param BenchmarkReport $benchmarkReport
   */
  public function print(BenchmarkReport $benchmarkReport);
}
