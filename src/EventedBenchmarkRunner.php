<?php declare(strict_types=1);

namespace XsolveBenchmark;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Enhances BenchmarkRunner by emitting events
 */
class EventedBenchmarkRunner extends BenchmarkRunner
{
  const PRE_RUN = 'benchmark.pre_run';
  const POST_RUN = 'benchmark.post_run';

  /**
   * @var EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * @param RequestStatsCollector $requestStatsCollector
   * @param EventDispatcherInterface $eventDispatcher
   */
  public function __construct(RequestStatsCollector $requestStatsCollector, EventDispatcherInterface $eventDispatcher)
  {
    parent::__construct($requestStatsCollector);
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public function run(array $uris)
  {
    $this->eventDispatcher->dispatch(self::PRE_RUN);
    $report = parent::run($uris);
    $this->eventDispatcher->dispatch(self::POST_RUN, new BenchmarkEvent($report));

    return $report;
  }
}
