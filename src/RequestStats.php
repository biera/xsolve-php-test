<?php declare(strict_types=1);

namespace XsolveBenchmark;

use GuzzleHttp\TransferStats;

/**
 * Thin wrapper around TransferStats
 */
class RequestStats
{
  /**
   * @var TransferStats
   */
  private $transferStats;

  /**
   * @param TransferStats $transferStats
   */
  public function __construct(TransferStats $transferStats)
  {
    $this->transferStats = $transferStats;
  }

  /**
   * @return string
   */
  public function getEffectiveUri() : string
  {
    return (string) $this->transferStats->getEffectiveUri();
  }

  /**
   * @return float
   */
  public function getTotalTime() : float
  {
    $this->assertNoNetworkErrorOccured();

    return $this->transferStats->getHandlerStat('total_time');
  }

  /**
   * @return float
   */
  public function getTransferTime() : float
  {
    $this->assertNoNetworkErrorOccured();

    return $this->transferStats->getTransferTime();
  }

  /**
   * @return float
   */
  public function getConnectTime() : float
  {
    $this->assertNoNetworkErrorOccured();

    return $this->transferStats->getHandlerStat('connect_time');
  }

  /**
   * @return float
   */
  public function getNamelookupTime() : float
  {
    $this->assertNoNetworkErrorOccured();

    return $this->transferStats->getHandlerStat('namelookup_time');
  }

  /**
   * @param string $stat
   *
   * @return mixed
   */
  public function getHandlerStat(string $stat)
  {
    return $this->transferStats->getHandlerStat($stat);
  }

  /**
   * @return bool
   */
  public function isNetworkError() : bool
  {
    return !$this->transferStats->hasResponse();
  }

  private function assertNoNetworkErrorOccured()
  {
    if ($this->isNetworkError()) {
      throw new \RuntimeException(
        sprintf('An error occured when sending a request (%s). Stats are not available', $this->transferStats->getEffectiveUri())
      );
    }
  }
}
