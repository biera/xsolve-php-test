<?php declare(strict_types=1);

namespace XsolveBenchmark\Printer;

use Symfony\Component\Console\Output\StreamOutput;
use XsolveBenchmark\BenchmarkReport;
use XsolveBenchmark\BenchmarkReportPrinter;

/**
 * Extends ConsolePrinter changing output to StreamOutput
 */
class FilePrinter extends ConsolePrinter
{
  /**
   * @var OutputInterface
   */
  private $output;

  /**
   * @var resource
   */
  private $stream;

  /**
   * @param string $output
   */
  public function __construct($path)
  {
    $this->stream = fopen($path, 'a');

    parent::__construct(
      new StreamOutput($this->stream)
    );
  }

  public function __destruct()
  {
    fclose($this->stream);
  }
}
