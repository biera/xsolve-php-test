<?php declare(strict_types=1);

namespace XsolveBenchmark\Printer;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Output\OutputInterface;
use XsolveBenchmark\BenchmarkReport;
use XsolveBenchmark\BenchmarkReportPrinter;

/**
 * Prints to OutputInterface (Symfony console)
 */
class ConsolePrinter implements BenchmarkReportPrinter
{
  /**
   * @var OutputInterface
   */
  private $output;

  /**
   * @param ConsoleOutput $output
   */
  public function __construct(OutputInterface $output)
  {
    $this->output = $output;
  }

  /**
   * @param BenchmarkReport $report
   */
  public function print(BenchmarkReport $report)
  {
    $this->output->writeln(
      'Ran at: ' . $report->getTime()->format('Y-m-d H:i:s')
    );

    (new Table($this->output))
      ->setHeaders(
        $this->getHeaders($report)
      )
      ->setRows(
        $this->getRows($report)
      )
      ->render();
  }

  private function getHeaders(BenchmarkReport $benchmarkReport)
  {
    $benchmarkTestTitles = $benchmarkReport->getBenchmarkTitles();

    $firstRowOfHeader = array_map(
      function ($benchmarkTestTitle) {
        return new TableCell("<comment>$benchmarkTestTitle</comment>", ['colspan' => 2]);
      },
      $benchmarkTestTitles
    );

    array_unshift($firstRowOfHeader, null);

    $secondRowOfHeader = ['URL'];

    for ($i = 0; $i < count($benchmarkTestTitles); $i++) {
      array_push($secondRowOfHeader, 'SCORE', 'COMPARISION');
    }

    return [$firstRowOfHeader, $secondRowOfHeader];
  }

  private function getRows(BenchmarkReport $benchmarkReport)
  {
    $results = $benchmarkReport->getResults();

    $rows = array_map(
      function ($url) use ($results) {
        $resultsForUriGroupedByBenchmarkTest = array_column($results, $url);

        return $this->getRow($url, $resultsForUriGroupedByBenchmarkTest);
      },
      $benchmarkReport->getBenchmarkURIs()
    );

    // highlight first row (benchmarked website) by coloring
    array_walk($rows[0], function(&$column) {
      $column = sprintf('<info>%s</info>', $column);
    });

    return $rows;
  }

  private function getRow($uri, array $resultsForUri)
  {
    return (null === array_values($resultsForUri)[0])
      ?
        // there are no results for that uri (network error occured while sending request)
        [$uri, new TableCell('<error>Network Error</error>', ['colspan' => count($resultsForUri) * 2])]
      :
        array_reduce(
          $resultsForUri,
          function ($reduced, $resultsByBenchmark) {
            array_push($reduced, $resultsByBenchmark['score']['formatted'], $resultsByBenchmark['comparision']['formatted']);

            return $reduced;
          },
          [$uri]
        );
  }
}
