<?php declare(strict_types=1);

namespace XsolveBenchmark\Printer;

use XsolveBenchmark\BenchmarkReportPrinter;
use XsolveBenchmark\BenchmarkReport;

class HtmlPrinter implements BenchmarkReportPrinter
{
  private $template;

  /**
   * @param string $template Path to template
   */
  public function __construct(string $template)
  {
    $this->template = $template;
  }

  public function print(BenchmarkReport $benchmarkReport)
  {
    include $this->template;
  }
}
