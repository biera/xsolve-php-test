<?php declare(strict_types=1);

namespace XsolveBenchmark;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Symfony Dependency Injection compiler pass
 *
 * It registers all container services tagged as 'benchmark' as BenchmarkTest to BenchmarkRunner
 */
class RegisterBenchmarkTestCompilerPass implements CompilerPassInterface
{
  const TAG = 'benchmark';

  /**
   * {@inheritDoc}
   */
  public function process(ContainerBuilder $container)
  {
    $benchmarkRunnerServiceDefinition = $container->getDefinition('benchmark_runner');

    foreach ($container->findTaggedServiceIds(self::TAG) as $benchmarkTestServiceId => $tagsSet) {
      foreach ($tagsSet as $tags) {
        if (!array_key_exists('title', $tags)) {
          throw new \LogicException(
            sprintf('The title tag is missing for %s service', $benchmarkTestServiceId)
          );
        }

        $benchmarkRunnerServiceDefinition
          ->addMethodCall(
            'registerBenchmarkTest', [$tags['title'], new Reference($benchmarkTestServiceId)]
          );
      }
    }
  }
}
