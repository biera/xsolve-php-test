<?php declare(strict_types=1);

namespace XsolveBenchmark\Logger;

/**
 * Dummy SMSGateway which sends messages to file
 */
class DummySMSGateway implements SMSGateway
{
  /**
   * @var string
   */
  private $path;

  /**
   * @param string $path path to file the message will be written to
   */
  public function __construct(string $path)
  {
    $this->path = $path;
  }

  /**
   * {@inheritDoc}
   */
  public function send(string $_, string $message)
  {
    file_put_contents($this->path, $message."\n", FILE_APPEND);
  }
}
