<?php declare(strict_types=1);

namespace XsolveBenchmark\Logger;

interface SMSGateway
{
  /**
   * Send a message to a given number
   *
   * @param string $number
   * @param string $message
   */
  public function send(string $number, string $message);
}
