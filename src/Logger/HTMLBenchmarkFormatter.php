<?php declare(strict_types=1);

namespace XsolveBenchmark\Logger;

use Monolog\Formatter\HtmlFormatter;
use XsolveBenchmark\BenchmarkReport;
use XsolveBenchmark\BenchmarkReportPrinter;

/**
 * Outputs log record as HTML (only when benchmark report provided)
 */
class HTMLBenchmarkFormatter extends HtmlFormatter
{
  /**
   * @var BenchmarkReportPrinter
   */
  private $benchmarkReportPrinter;

  /**
   * @param BenchmarkReportPrinter $benchmarkReportPrinter
   */
  public function __construct(BenchmarkReportPrinter $benchmarkReportPrinter)
  {
    parent::__construct('Y-m-d H:i:s');
    $this->benchmarkReportPrinter = $benchmarkReportPrinter;
  }

  /**
   * {@inheritDoc}
   */
  public function format(array $record)
  {
    $formatted = parent::format($record);

    if (array_key_exists('benchmark_report', $record['context']) && ($report = $record['context']['benchmark_report']) instanceof BenchmarkReport) {
      // workaround... because benchmarkReportPrinter::print() does't return but outputs
      ob_start();
      $this->benchmarkReportPrinter->print($report);
      $formatted .= ob_get_clean();
    }

    return $formatted;
  }

  /**
   * {@inheritDoc}
   */
  public function formatBatch(array $records)
  {
    return array_map($this->format, $records);
  }
}
