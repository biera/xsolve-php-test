<?php declare(strict_types=1);

namespace XsolveBenchmark\Logger;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;

/**
 * Write logs to SMS Gateway (sends logs via SMS)
 */
class SMSHandler extends AbstractProcessingHandler
{
  /**
   * @var SMSGateway
   */
  private $smsGateway;

  /**
   * @param SMSGateway $smsGateway
   * @param string $number
   */
  public function __construct(SMSGateway $smsGateway, string $number, string $level)
  {
    parent::__construct($level);
    $this->smsGateway = $smsGateway;
    $this->number = $number;
  }

  /**
   * {@inheritDoc}
   */
  protected function write(array $record)
  {
    $this->setBubble(true);

    // log record context must set SMS flag to true
    if (array_key_exists('sms', $record['context']) && $record['context']['sms']) {
      $this->smsGateway->send(
        $this->number, $record['formatted']
      );
      $this->setBubble(false);
    }
  }
}
