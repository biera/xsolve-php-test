<?php

namespace XsolveBenchmark\DI;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Just to ensure all required parameters as present
 */
class BenchmarkExtension extends Extension
{
  public function load(array $configs, ContainerBuilder $container)
  {
    if (!defined('ROOT_DIR')) {
      define('ROOT_DIR', __DIR__.'/../../');
    }

    $configuration = new Configuration();
    $config = $this->processConfiguration($configuration, $configs);

    $container->setParameter(
      'report_path', $this->conditionallyPrependPath($config['report']['path'])
    );

    $logsConfig = $config['logs'];
    $container->setParameter(
      'logs_file_path', $this->conditionallyPrependPath($logsConfig['file']['path'])
    );
    $container->setParameter(
      'logs_sms_path', $this->conditionallyPrependPath($logsConfig['sms']['path'])
    );
    $container->setParameter(
      'logs_mail_template_path', $this->conditionallyPrependPath($logsConfig['mail']['template_path'])
    );
    $container->setParameter('logs_sms_receiver', $logsConfig['sms']['receiver']);
    $container->setParameter('logs_mail_receiver', $logsConfig['mail']['receiver']);
    $container->setParameter('logs_mail_sender', $logsConfig['mail']['sender']);

    $smtpConfig = $logsConfig['mail']['smtp_config'];
    $container->setParameter('logs_mail_smtp_config_host', $smtpConfig['host']);
    $container->setParameter('logs_mail_smtp_config_port', $smtpConfig['port']);
    $container->setParameter('logs_mail_smtp_config_credentials_user', $smtpConfig['credentials']['user']);
    $container->setParameter('logs_mail_smtp_config_credentials_password', $smtpConfig['credentials']['password']);
  }

  /**
   * @param string $path
   *
   * @return string
   */
  private function conditionallyPrependPath($path)
  {
    if (!$this->isAbsoultePath($path)) {
      $path = ROOT_DIR.$path;
    }

    return $path;
  }

  /**
   * @param string $path
   *
   * @return bool
   */
  private function isAbsoultePath($path)
  {
    return substr($path, 0, 1) === DIRECTORY_SEPARATOR;
  }
}

// intentionally private, not available for psr autoloader
class Configuration implements ConfigurationInterface
{
  public function getConfigTreeBuilder()
  {
    $treeBuilder = new TreeBuilder();
    $treeBuilder
      ->root('benchmark')
      ->children()
        ->arrayNode('report')
          ->isRequired()
          ->children()
            ->scalarNode('path')
              ->isRequired()
            ->end()
          ->end()
        ->end()
        ->arrayNode('logs')
          ->isRequired()
          ->children()
            ->arrayNode('file')
              ->isRequired()
              ->children()
                ->scalarNode('path')
                  ->isRequired()
                ->end()
              ->end()
            ->end()
            ->arrayNode('sms')
              ->isRequired()
              ->children()
                ->scalarNode('receiver')
                  ->isRequired()
                ->end()
                ->scalarNode('path')
                  ->isRequired()
                ->end()
              ->end()
            ->end()
            ->arrayNode('mail')
              ->isRequired()
              ->children()
                ->scalarNode('receiver')
                  ->isRequired()
                ->end()
                ->scalarNode('sender')
                  ->isRequired()
                ->end()
                ->scalarNode('template_path')
                  ->isRequired()
                ->end()
                ->arrayNode('smtp_config')
                  ->isRequired()
                  ->children()
                    ->scalarNode('host')
                      ->isRequired()
                    ->end()
                    ->scalarNode('port')
                      ->isRequired()
                    ->end()
                    ->arrayNode('credentials')
                      ->isRequired()
                      ->children()
                        ->scalarNode('user')
                          ->isRequired()
                        ->end()
                        ->scalarNode('password')
                          ->isRequired()
                        ->end()
                      ->end()
                    ->end()
                  ->end()
                ->end()
              ->end()
            ->end()
          ->end()
        ->end()
      ->end()
    ;

    return $treeBuilder;
  }
}
