<?php declare(strict_types=1);

namespace XsolveBenchmark\Formatter;

use XsolveBenchmark\BenchmarkTestFormatter;

class DefaultFormatter implements BenchmarkTestFormatter
{
  /**
   * Formats float numbers as time in seconds
   */
  public function formatScore($score) : string
  {
    return sprintf('%.5fs', $score);
  }

  /**
   * Formats ratio as percentage
   */
  public function formatComparision($comparision) : string
  {
    return sprintf('%.2f%s', $comparision * 100, '%');
  }

}
