<?php declare(strict_types=1);

namespace XsolveBenchmark;

interface BenchmarkTest
{
  /**
   * @return mixed
   */
  public function extractScore(RequestStats $stats);

  /**
   * @return mixed
   */
  public function compareScores($referenceScore, $otherScore);

  /**
   * @return BenchmarkFormatter
   */
  public function getFormatter() : BenchmarkTestFormatter;
}
