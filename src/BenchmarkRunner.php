<?php declare(strict_types=1);

namespace XsolveBenchmark;

/**
 * Runs all registred benchmark tests and returns report
 */
class BenchmarkRunner
{
  /**
   * @var BenchmarkTest[]
   */
  private $benchmarkTests;

  /**
   * @param RequestStatsCollector $requestStatsCollector
   */
  public function __construct(RequestStatsCollector $requestStatsCollector)
  {
    $this->requestStatsCollector = $requestStatsCollector;
    $this->benchmarkTests = [];
  }

  /**
   * Register benchmark test
   *
   * @param string $title benchmark test title
   * @param BenchmarkTest $benchmarkTest benchmark test
   */
  public function registerBenchmarkTest(string $title, BenchmarkTest $benchmarkTest)
  {
    $this->benchmarkTests[$title] = $benchmarkTest;
  }

  /**
   * Collects scores and compare them
   * with reference score (socre for benchmarked website)
   * for each benchmark test
   *
   * @param array $uris
   *
   * @return BenchmarkReport
   */
  public function run(array $uris)
  {
    $requestStats = $this->requestStatsCollector->sendAndCollect($uris);

    return new BenchmarkReport(
      array_map(
        function ($benchmarkTest) use ($requestStats) {
          return $this->runBenchmark($benchmarkTest, $requestStats);
        },
        $this->benchmarkTests
      )
    );
  }

  /**
   * Collects scores and compare them
   * with reference score (socre for benchmarked website)
   * for given benchmark test
   *
   * @param BenchmarkTest $benchmarkTest benchmark test instance
   * @param array $stats a collection of RequestStats
   *
   * @return array
   */
  private function runBenchmark(BenchmarkTest $benchmarkTest, array $requestsStatsCollection)
  {
    $benchmarkedWebsiteRequestStats = $this->getBenchmarkedWebsiteRequestStats($requestsStatsCollection);

    if ($benchmarkedWebsiteRequestStats->isNetworkError()) {
      // if reference score is not available further processing isn't possible
      throw new \RuntimeException('Reference score is not available (due to network error).');
    }

    $referenceScore = $benchmarkTest->extractScore($benchmarkedWebsiteRequestStats);
    $formatter = $benchmarkTest->getFormatter();

    return array_map(
      function (RequestStats $requestStats) use ($benchmarkTest, $referenceScore, $formatter) {
        return $requestStats->isNetworkError()
          ?
            null
          :
            [
              'score' => [
                'raw' => $score = $benchmarkTest->extractScore($requestStats),
                'formatted' => $formatter->formatScore($score)
              ],
              'comparision' => [
                'raw' => $comparision = $benchmarkTest->compareScores($referenceScore, $score),
                'formatted' => $formatter->formatComparision($comparision)
              ]
            ];
      },
      $requestsStatsCollection
    );
  }

  private function getBenchmarkedWebsiteRequestStats(array $requestsStatsCollection)
  {
    // the benchmarked website is always the first one
    return array_values($requestsStatsCollection)[0];
  }
}
