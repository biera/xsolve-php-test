<?php declare(strict_types=1);

namespace XsolveBenchmark;

/**
 * Wraps an array with benchmark tests results providing useful methods to extract report data
 */
class BenchmarkReport
{
  /**
   * @var \DateTime
   */
  private $time;

  /**
   * An array of the following shape
   *
   * [
   *  benchmark_test_1 =>
   *    [
   *      uri1 => [ 'score' => SCORE, 'comparision' => COMPARISION ],
   *      uri2 => [ 'score' => SCORE, 'comparision' => COMPARISION ],
   *      ...
   *    ],
   *  ...
   * ]
   *
   * @var array
   */
  private $benchmarkResults;

  /**
   * @param array $benchmarkResults
   */
  public function __construct(array $benchmarkResults)
  {
    $this->benchmarkResults = $benchmarkResults;
    $this->time = new \DateTime();
  }

  /**
   * @return \DateTime
   */
  public function getTime()
  {
    return $this->time;
  }

  /**
   * @return array
   */
  public function getBenchmarkTitles()
  {
    return array_keys($this->benchmarkResults);
  }

  /**
   * @return array
   */
  public function getBenchmarkURIs()
  {
    return array_keys(
      array_values($this->benchmarkResults)[0]
    );
  }

  /**
   * Gives results for given benchmark test,
   * returns an array of the following shape:
   *
   *  [
   *    uri1 => [ 'score' => SCORE, 'comparision' => COMPARISION ],
   *    uri2 => [ 'score' => SCORE, 'comparision' => COMPARISION ],
   *    ...
   *  ]
   *
   * or throws exception when non existen benchmark test requested.
   *
   * @param string $benchmarkTitle
   * @throws \LogicException when no benchmark with given $benchmarkTitle was registred
   *
   * @return array
   */
  public function getResultsByBenchmark(string $benchmarkTitle)
  {
    if (!array_key_exists($benchmarkTitle, $this->benchmarkResults)) {
      throw new \LogicException(sprintf('Benchmark test with %s title has not been registered', $benchmarkTitle));
    }

    return $this->benchmarkResults[$benchmarkTitle];
  }

  /**
   * Gets an array of shape
   *
   * [
   *   uri1 => [
   *     benchmark1 => [
   *       'score' => SCORE, 'comparision' => COMPARISION
   *     ]
   *     benchmark2 => [
   *       'score' => SCORE, 'comparision' => COMPARISION
   *     ],
   *     ...
   *   ],
   *   uri2 => ...
   * ]
   *
   * @return array
   */
  public function getResultsGroupedByURI()
  {
    $uris = $this->getBenchmarkURIs();
    $titles = $this->getBenchmarkTitles();

    $records = array_map(
      function ($url) use ($titles) {
        $resultsForUriGroupedByBenchmarkTest = array_column($this->benchmarkResults, $url);

        return array_combine($titles, $resultsForUriGroupedByBenchmarkTest);
      },
      $uris
    );

    return array_combine($uris, $records);
  }

  /**
   * Gives results for all benchmark tests
   *
   * @return array
   */
  public function getResults()
  {
    return $this->benchmarkResults;
  }
}
