<?php declare(strict_types=1);

namespace XsolveBenchmark\Benchmark;

use XsolveBenchmark\RequestStats;
use XsolveBenchmark\BenchmarkTest;
use XsolveBenchmark\BenchmarkTestFormatter;

/**
 * Tests HTTP endpoint response time
 */
class ResponseTimeBenchmark implements BenchmarkTest
{
  /**
   * @var BenchmarkFormatter
   */
  private $formatter;

  /**
   * @param BenchmarkFormatter $formatter
   */
  public function __construct(BenchmarkTestFormatter $formatter)
  {
    $this->formatter = $formatter;
  }

  /**
   * {@inheritDoc}
   */
  public function extractScore(RequestStats $stats)
  {
    return $stats->getTransferTime();
  }

  /**
   * {@inheritDoc}
   */
  public function compareScores($reference, $other)
  {
    return $other / $reference;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormatter() : BenchmarkTestFormatter
  {
    return $this->formatter;
  }
}
