<?php declare(strict_types=1);

namespace XsolveBenchmark;

interface BenchmarkTestFormatter
{
  /**
   * @param mixed $score
   *
   * @return string
   */
  public function formatScore($score) : string;

  /**
   * @param mixed $comparision
   *
   * @return string
   */
  public function formatComparision($comparision) : string;
}
