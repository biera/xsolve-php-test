<?php

namespace XsolveBenchmark;

use Symfony\Component\EventDispatcher\Event;

class BenchmarkEvent extends Event
{
  /**
   * @var array
   */
  private $report;

  /**
   * @param array $report
   */
  public function __construct($report)
  {
    $this->report = $report;
  }

  /**
   * @return array $report
   */
  public function getReport()
  {
    return $this->report;
  }
}
