<?php

namespace helpers;

/**
 * It sorts the array according to the order provided by $keysOrder param
 *
 * Usage:
 *  uksort($array, sort_by_keys($keysOrder))
 *
 * E.g:
 *  $array = ['c' => 3, 'a' => 1, 'b' => 2 ]
 *  uksort($array, sort_by_keys(['a', 'b', 'c']))
 *  var_dump($array) // ['a' => 1, 'b' => 2, 'c' => 3 ]
 *
 * Note:
 *  $keysOrder has to have exactly the same elements as sorted $array
 *  and includes all $array keys, otherwise the results are not predictable
 *
 */
function sort_by_keys($keysOrder)
{
  return function($keyA, $keyB) use ($keysOrder)
  {
    return array_search($keyA, $keysOrder) <=> array_search($keyB, $keysOrder);
  };
}
