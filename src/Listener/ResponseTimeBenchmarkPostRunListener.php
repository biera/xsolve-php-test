<?php declare(strict_types=1);

namespace XsolveBenchmark\Listener;

use XsolveBenchmark\BenchmarkEvent;
use XsolveBenchmark\BenchmarkReport;
use Psr\Log\LoggerInterface;
use iter, iter\fn;

/**
 * Listens for benchmark.posr_run event
 */
class ResponseTimeBenchmarkPostRunListener
{
  const BENCHMARK_NAME = 'ResponseTime';

  /**
   * comparision value is expressed in ratio: response time / reference response time
   */
  const EMAIL_ALERT_THRESHOLD = 1.0;

  const EMAIL_ALERT_MESSAGE = 'At least one webpage was loaded faster than benchmarked one.';

  /**
   * comparision value is expressed in ratio: response time / reference response time
   */
  const SMS_ALERT_THRESHOLD = 0.5;

  const SMS_ALERT_MESSAGE = 'At least one webpage was loaded twice faster than benchmarked one.';

  /**
   * @var LoggerInterface
   */
  private $logger;

  /**
   * @param LoggerInterface
   */
  public function __construct(LoggerInterface $logger)
  {
    $this->logger = $logger;
  }

  /**
   *@param BenchmarkEvent $event
   */
  public function onBenchmarkPostRun(BenchmarkEvent $event)
  {
    $this->sendAlertWhenThresholdExceeded(
      $event->getReport()
    );
  }

  /**
   * Send an alert via email when threshold exceeded
   */
  private function sendAlertWhenThresholdExceeded(BenchmarkReport $report)
  {
    $timeResponseBenchmarkResults = $report->getResultsByBenchmark(self::BENCHMARK_NAME);

    $rawComparisions = array_map(
      function ($comparision) {
        return $comparision['raw'];
      },
      array_column($timeResponseBenchmarkResults, 'comparision')
    );

    if (iter\any(fn\operator('<', self::EMAIL_ALERT_THRESHOLD), $rawComparisions)) {
     $this->logger->warning(self::EMAIL_ALERT_MESSAGE, ['benchmark_report' => $report]);
    }

    if (iter\any(fn\operator('<', self::SMS_ALERT_THRESHOLD), $rawComparisions)) {
      $this->logger->warning(self::SMS_ALERT_MESSAGE, ['sms' => true]);
    }
  }
}
