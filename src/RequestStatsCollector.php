<?php declare(strict_types=1);

namespace XsolveBenchmark;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Promise;
use helpers;

/**
 * Sends HTTP GET requests and collects stats
 */
class RequestStatsCollector
{
  /**
   * @var ClientInterface
   */
  private $httpClient;

  /**
   * @var float
   */
  private $connectionTimeout;

  /**
   * @var RequestStats[]
   */
  private $requestStats;

  /**
   * @param ClientInterface $httpClient
   * @param float $httpClient
   */
  public function __construct(ClientInterface $httpClient, float $connectionTimeout = 10)
  {
    $this->httpClient = $httpClient;
    $this->connectionTimeout = $connectionTimeout;
  }

  /**
   * Sends GET requests to each uri provided in first param,
   * returns a map uri => RequestStats
   *
   * @param array $uris
   *
   * @return array
   */
  public function sendAndCollect(array $uris) : array
  {
    // do nothing with responses, we are interested in stats
    Promise\settle($this->urisToRequests($uris))->wait();

    // retain the original order (request are sent in async mode)
    uksort($this->requestStats, helpers\sort_by_keys($uris));

    return $this->requestStats;
  }

  private function urisToRequests($uris)
  {
    $client = $this->httpClient;

    return array_map(
      function ($uri) use ($client) {
        return $client->getAsync(
          $uri,
          [
            RequestOptions::ALLOW_REDIRECTS => false,
            RequestOptions::CONNECT_TIMEOUT => $this->connectionTimeout,
            RequestOptions::ON_STATS => function (TransferStats $transferStats) {
              $this->requestStats[(string) $transferStats->getEffectiveUri()] = new RequestStats($transferStats);
            }
          ]
        );
      },
      $uris
    );
  }
}
