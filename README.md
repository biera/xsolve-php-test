# Benchmark
### Install
---
Clone the project and install its deps via composer
```bash
git clone https://biera@bitbucket.org/biera/xsolve-php-test.git && cd xsolve-php-test && composer install
```

### Test
---
Run **PHPspec** and **PHPUnit** tests
```bash
bin/phpspec run ; bin/phpunit --color tests/helpers.php
```
### Run
---
Run the benchmark
```bash
bin/benchmark benchmarked_uri [another_uri1, [another_uri2, ...]]
```
For example
```bash
bin/benchmark xsolve.software www.youtube.pl www.pudelek.pl
```
Report will be written directly to the console and aditionally to the file (**PROJECT_DIR/var/report.log** by default).

#### Error handling

* if network error occurs (e.g name lookup failed) for **benchmarked_uri** the app exits immediately.
* if network error occurs (e.g name lookup failed) for **another_uriX** the app simply skips extracting score for **another_uriX** and comparing it with reference score (score for **benchmarked_uri**)
* HTTP errors (status codes from 4xx and 5xx families) are not considered errors here, app keeps extracting score and compares it with reference score

#### Logs
* logs with at least WARNING level are written to INBOXES and SMS Gateway
    * INBOX: benchmark some websites and check the inbox for alerts on https://mailtrap.io using the following credentials => username: **kubiernacki+mailtrap@gmail.com**; password: **password** (check the **"Demo inbox"**)
    * SMS: app uses DummySMSGateway which is an implementation of SMSGateway that actually writes logs to the file (PROJECT_ROOT/var/sms.log by default)

* logs with other level are written to file (**PROJECT_DIR/var/app.log**)

### Extend
---
#### Add new **Benchmark Test**
Provide implementation of **XsolveBenchmark\BenchmarkTest** (used to extract the score and compare it with other scores). Alternatively, implement **XsolveBenchmark\BenchmarkTestFormatter** (used to format scores and comparisions) to format benchmark results:
```php
namespace XsolveBenchmark\Benchmark

use XsolveBenchmark\RequestStats;
use XsolveBenchmark\BenchmarkTest;
use XsolveBenchmark\BenchmarkTestFormatter

class ConnectTimeBenchmark
{
    private $formatter;

    public function __construct(BenchmarkTestFormatter $formatter)
    {
      $this->formatter = $formatter;
    }

    public function extractScore(RequestStats $stats)
    {
      return $stats->getConnectTime();
    }

    public function compareScores($reference, $other)
    {
      return $other / $reference;
    }

    public function getFormatter() : BenchmarkTestFormatter
    {
      return $this->formatter;
    }
}
```

* **XsolveBenchmark\BenchmarkTest::extractScore()** receives as an argument object of **XsolveBenchmark\RequestStats** class which contains request's stats (e.g tranfser time, connect time and so on). The only purpose of this method is to extract interesting (in the context of benchmark test) value from that object, e.g connect time **(XsolveBenchmark\RequestStats::getConnectTime()**).

* **XsolveBenchmark\BenchmarkTest::compareScores()** receives two scores, the reference score (score extracted for **benchmarked_uri**) and other score. The task of this method is to give information about how the other scores relates to the reference score. It could be anything, in our case it's a ratio (connect time of website) / (connect time of benchmarked website).

The next step is to register the **Benchmark Test** to **Benchmark Runner**. To do so, register new service to DI container by editing **PROJECT_DIR/etc/config.yml** file, like this:
```
services:
  connect_time_benchmark:
    class: XsolveBenchmark\Benchmark\ConnectTimeBenchmark
    arguments: ['@default_formatter']
    tags:
      - { name: benchmark, title: ConnectTime }
```
Things to note:

* we use existing service **default_formatter** which fits well in this case
* we tag the service as **benchmark**, additionally providing **title**

#### Listen for emitted events
There are two events emitted to listen for:

* **benchmark.pre_run** - emitted before running benchmark tests
* **benchmark.post_run** - emitted after running benchmark tests

The listener for **benchmark.post_run** receives **XsolveBenchmark\BenchmarkEvent** object which holds Benchmark Report (**XsolveBenchmark\BenchmarkReport**).

Again, to register a new listener you need to register a new service to DI container. Here is an example of such service definition (BTW, it's a standard way to implement listeners in Symfony app, read about it [here](http://symfony.com/doc/current/event_dispatcher.html)):
```
services:
  repsonse_time_benchmark_post_run_listener:
    class: XsolveBenchmark\Listener\ResponseTimeBenchmarkPostRunListener
    arguments: ['@logger']
    tags:
      - { name: kernel.event_listener, event: benchmark.post_run }
```

Take a look at **XsolveBenchmark\Listeners\ResponseTimeBenchmarkPostRunListener** to see how alerts are sent when given thresholds are exceeded in ResponseTime benchmark test.