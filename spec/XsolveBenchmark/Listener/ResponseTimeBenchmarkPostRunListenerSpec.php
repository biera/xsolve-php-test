<?php

namespace spec\XsolveBenchmark\Listener;

use XsolveBenchmark\Listener\ResponseTimeBenchmarkPostRunListener;
use XsolveBenchmark\BenchmarkReport;
use XsolveBenchmark\BenchmarkEvent;
use Psr\Log\LoggerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ResponseTimeBenchmarkPostRunListenerSpec extends ObjectBehavior
{
  public function let(LoggerInterface $logger)
  {
    $this->beConstructedWith($logger);
  }

  public function it_is_initializable()
  {
    $this->shouldHaveType(ResponseTimeBenchmarkPostRunListener::class);
  }

  public function it_sends_alert_when_some_website_was_loaded_faster_than_benchmarked_one(LoggerInterface $logger)
  {
    $report = new BenchmarkReport(
      [
        ResponseTimeBenchmarkPostRunListener::BENCHMARK_NAME => [
          'benchmarked_uri' => [
            'comparision' => [
              'raw' => 1.0 // it's always 1.0
            ]
          ],
          'other_uri' => [
            'comparision' => [
              'raw' => 0.9 // the other_uri was loaded faster!
            ]
          ]
        ]
      ]
    );

    $this->onBenchmarkPostRun(
      new BenchmarkEvent($report)
    );

    $logger->warning(ResponseTimeBenchmarkPostRunListener::EMAIL_ALERT_MESSAGE, ['benchmark_report' => $report])->shouldBeCalled();
  }

  public function it_sends_alert_when_some_website_was_loaded_twice_faster_than_benchmarked_one(LoggerInterface $logger)
  {
    $report = new BenchmarkReport(
      [
        ResponseTimeBenchmarkPostRunListener::BENCHMARK_NAME => [
          'benchmarked_uri' => [
            'comparision' => [
              'raw' => 1.0 // it's always 1.0
            ]
          ],
          'other_uri' => [
            'comparision' => [
              'raw' => 0.49 // the other_uri was loaded twice faster!
            ]
          ]
        ]
      ]
    );

    $this->onBenchmarkPostRun(
      new BenchmarkEvent($report)
    );

    $logger->warning(ResponseTimeBenchmarkPostRunListener::SMS_ALERT_MESSAGE, ['sms' => true])->shouldBeCalled();
  }
}
