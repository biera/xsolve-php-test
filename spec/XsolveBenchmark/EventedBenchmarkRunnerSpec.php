<?php

namespace spec\XsolveBenchmark;

use XsolveBenchmark\EventedBenchmarkRunner;
use XsolveBenchmark\BenchmarkEvent;
use XsolveBenchmark\RequestStatsCollector;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class EventedBenchmarkRunnerSpec extends ObjectBehavior
{
  const URIS = [
    'xsolve.software', 'www.youtube.pl'
  ];

  public function let(RequestStatsCollector $requestStatsCollector, EventDispatcherInterface $eventDispatcher)
  {
    $requestStatsCollector->sendAndCollect(self::URIS)->willReturn([]);
    $this->beConstructedWith($requestStatsCollector, $eventDispatcher);
  }

  public function it_is_initializable()
  {
    $this->shouldHaveType(EventedBenchmarkRunner::class);
  }

  public function it_emit_events_before_and_after_run(EventDispatcherInterface $eventDispatcher)
  {
    $eventDispatcher->dispatch(EventedBenchmarkRunner::PRE_RUN)->shouldBeCalled();
    $eventDispatcher->dispatch(EventedBenchmarkRunner::POST_RUN, Argument::type(BenchmarkEvent::class))->shouldBeCalled();

    $this->run(self::URIS);
  }
}
