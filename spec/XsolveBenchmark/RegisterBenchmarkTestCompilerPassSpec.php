<?php

namespace spec\XsolveBenchmark;

use XsolveBenchmark\RegisterBenchmarkTestCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RegisterBenchmarkTestCompilerPassSpec extends ObjectBehavior
{
  const SERVICE_ID = 'some_benchmark_test_service';
  const SERVICE_TAGS = [
    ['title' => 'ResponseTimeBenchmark']
  ];

  public function let(ContainerBuilder $containerBuilder, Definition $benchmarkRunnerServiceDefinition)
  {
    $containerBuilder->getDefinition('benchmark_runner')->willReturn($benchmarkRunnerServiceDefinition);
  }

  public function it_is_initializable()
  {
    $this->shouldHaveType(RegisterBenchmarkTestCompilerPass::class);
  }

  public function it_should_register_all_benchmark_test_services_tagged_as_benchmark(
    ContainerBuilder $containerBuilder,
    Definition $benchmarkRunnerServiceDefinition
  )
  {
    $containerBuilder->findTaggedServiceIds(RegisterBenchmarkTestCompilerPass::TAG)->willReturn(
      [self::SERVICE_ID => self::SERVICE_TAGS]
    );

    $this->process($containerBuilder);

    $benchmarkRunnerServiceDefinition
      ->addMethodCall(
        'registerBenchmarkTest', [self::SERVICE_TAGS[0]['title'], new Reference(self::SERVICE_ID)]
      )
      ->shouldHaveBeenCalled();
  }

  public function it_should_throw_exception_when_title_tag_is_missing(ContainerBuilder $containerBuilder)
  {
    $tagsWithoutTitle = [];

    $containerBuilder->findTaggedServiceIds(RegisterBenchmarkTestCompilerPass::TAG)->willReturn(
      [self::SERVICE_ID => [$tagsWithoutTitle]]
    );

    $this->shouldThrow(\LogicException::class)->during('process', [$containerBuilder]);
  }
}
