<?php

namespace spec\XsolveBenchmark;

use XsolveBenchmark\BenchmarkRunner;
use XsolveBenchmark\BenchmarkReport;
use XsolveBenchmark\BenchmarkTest;
use XsolveBenchmark\BenchmarkTestFormatter;
use XsolveBenchmark\RequestStats;
use XsolveBenchmark\RequestStatsCollector;
use PhpSpec\ObjectBehavior;
use PhpSpec\Exception\Example\FailureException;
use Prophecy\Argument;

class BenchmarkRunnerSpec extends ObjectBehavior
{
    const BENCHMARK_TEST_TITLE = 'benchmark_test';
    const URIS = [
      'xsolve.software', 'www.youtube.pl'
    ];

    public function let(RequestStatsCollector $requestStatsCollector, RequestStats $requestStats)
    {
      $requestStats->isNetworkError()->willReturn(false);
      $requestStatsCollector->sendAndCollect(self::URIS)->willReturn(
        array_combine(self::URIS, [$requestStats, $requestStats])
      );

      $this->beConstructedWith($requestStatsCollector);
    }

    public function it_is_initializable()
    {
      $this->shouldHaveType(BenchmarkRunner::class);
    }

    public function it_produces_empty_report_when_no_bechmark_test_registred()
    {
      $report = $this->run(self::URIS);
      $report->shouldBeAnInstanceOf(BenchmarkReport::class);
      $report->shouldBeEmpty();
    }

    public function it_produces_report_for_each_registred_benchmark_tests()
    {
      $this->registerBenchmarkTest(self::BENCHMARK_TEST_TITLE, $this->getDummyBenchmarkTest());

      $report = $this->run(self::URIS);
      $report->shouldBeAnInstanceOf(BenchmarkReport::class);
      $report->shouldHaveResultsForRegisteredBenchmark(self::BENCHMARK_TEST_TITLE);
    }

    public function it_throws_exception_when_error_network_occurs_for_first_website(
      RequestStatsCollector $requestStatsCollector,
      RequestStats $requestStatsForBenchmarkedURI,
      RequestStats $requestStats
    )
    {
      $requestStatsForBenchmarkedURI->isNetworkError()->willReturn(true);
      $requestStatsCollector->sendAndCollect(self::URIS)->willReturn(
        array_combine(self::URIS, [$requestStatsForBenchmarkedURI, $requestStats])
      );
      $this->beConstructedWith($requestStatsCollector);

      $this->registerBenchmarkTest(self::BENCHMARK_TEST_TITLE, $this->getDummyBenchmarkTest());
      $this->shouldThrow(\RuntimeException::class)->during('run', [self::URIS]);
    }

    public function it_set_null_as_result_when_error_network_occurs_for_non_first_uri(
      RequestStatsCollector $requestStatsCollector,
      RequestStats $requestStats,
      RequestStats $requestStatsForNonBenchmarkedURI
    )
    {
      $requestStatsForNonBenchmarkedURI->isNetworkError()->willReturn(true);
      $requestStatsCollector->sendAndCollect(self::URIS)->willReturn(
        array_combine(self::URIS, [$requestStats, $requestStatsForNonBenchmarkedURI])
      );

      $this->registerBenchmarkTest(self::BENCHMARK_TEST_TITLE, $this->getDummyBenchmarkTest());
      $this->run(self::URIS)->shouldHaveNullAsBenchmarkTestResult(self::BENCHMARK_TEST_TITLE, self::URIS[1]);
    }

    public function getMatchers()
    {
      return [
        'beEmpty' => function($benchmarkReport) {
          if ($benchmarkReport->getResults() !== []) {
            throw new FailureException('Report must be empty when no benchmark test registred');
          }

          return true;
        },
        'haveResultsForRegisteredBenchmark' => function ($benchmarkReport, $benchmarkTitle)  {
          try {
            $benchmarkReport->getResultsByBenchmark($benchmarkTitle);
          } catch (\LogicException $e) {
            throw new FailureException('There are no results for registred benchmark test');
          }

          return true;
        },
        'haveNullAsBenchmarkTestResult' => function ($benchmarkReport, $benchmarkTitle, $uri) {
          if(!is_null($benchmarkReport->getResultsByBenchmark(self::BENCHMARK_TEST_TITLE)[$uri])) {
            throw new FailureException('The result should be null when network error occured');
          }

          return true;
        }
      ];
    }

    private function getDummyBenchmarkTest()
    {
      return new class() implements BenchmarkTest
      {
        const DUMMY_SCORE = 1;
        const DUMMY_COMPARISION = 1;

        function extractScore(RequestStats $_)
        {
          return self::DUMMY_SCORE;
        }

        function compareScores($_1, $_2)
        {
          return self::DUMMY_COMPARISION;
        }

        function getFormatter() : BenchmarkTestFormatter
        {
            return new class implements BenchmarkTestFormatter
            {
              function formatScore($score) : string
              {
                return $score;
              }

              function formatComparision($comparision) : string
              {
                return $comparision;
              }
            };
        }
      };
    }
}
