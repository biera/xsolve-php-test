<?php

namespace spec\XsolveBenchmark;

use XsolveBenchmark\RequestStatsCollector;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\Promise;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Exception\RequestException;
use PhpSpec\Exception\Example\FailureException;
use iter, iter\fn;

class RequestStatsCollectorSpec extends ObjectBehavior
{
  const URIS = [
    'xsolve.software', 'www.youtube.pl'
  ];

  public function let()
  {
    $this->beConstructedWith(
      $this->getClient()
    );
  }

  public function it_is_initializable()
  {
    $this->shouldHaveType(RequestStatsCollector::class);
  }

  public function it_sends_request_and_collects_stats_for_each_uri()
  {
    $requestStats = $this->sendAndCollect(self::URIS);

    foreach (self::URIS as $uri) {
      $requestStats->shouldHaveKey($uri);
    }
  }

  /**
   * Get client with mock handler (no HTTP request is actually send)
   */
  private function getClient()
  {
    $handlerStack = HandlerStack::create(
      new MockHandler([new Response(200), new Response(200)])
    );

    return new Client(['handler' => $handlerStack]);
  }
}
