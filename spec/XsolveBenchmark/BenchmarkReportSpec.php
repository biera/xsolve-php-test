<?php

namespace spec\XsolveBenchmark;

use XsolveBenchmark\BenchmarkReport;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BenchmarkReportSpec extends ObjectBehavior
{
  const BENCHMARK_TEST_TITLE = 'ConnectTime';
  const URIS = [
    'xsolve.software', 'www.youtube.pl'
  ];
  const RAW_REPORT = [
    self::BENCHMARK_TEST_TITLE => [
      self::URIS[0] => ['SCORE_AND_COMPARISION_ARE_IRRELEVANT_HERE'],
      self::URIS[1] => ['SCORE_AND_COMPARISION_ARE_IRRELEVANT_HERE']
    ]
  ];

  public function let()
  {
    $this->beConstructedWith(self::RAW_REPORT);
  }

  public function it_is_initializable()
  {
    $this->shouldHaveType(BenchmarkReport::class);
  }

  public function it_gives_results_for_registred_benchmark_test_requested()
  {
    $benchmarkTestReport = $this->getResultsByBenchmark(self::BENCHMARK_TEST_TITLE);

    $benchmarkTestReport->shouldHaveKey(self::URIS[0]);
    $benchmarkTestReport->shouldHaveKey(self::URIS[1]);
  }

  public function it_throws_exception_when_results_for_non_registred_benchmark_test_requested()
  {
    $nonRegistredBenchmarkTestTitle = 'NamelookupTime';

    $this->shouldThrow(\LogicException::class)->during('getResultsByBenchmark', [$nonRegistredBenchmarkTestTitle]);
  }

  public function it_gives_all_tested_uris()
  {
    $benchmarkedWebsites = $this->getBenchmarkURIs();

    $benchmarkedWebsites->shouldContain(self::URIS[0]);
    $benchmarkedWebsites->shouldContain(self::URIS[1]);
  }

  public function it_gives_all_benchmark_titles()
  {
    $this->getBenchmarkTitles()->shouldContain(self::BENCHMARK_TEST_TITLE);
  }

  public function it_gives_all_results_provided_during_construction()
  {
    $this->getResults()->shouldBe(self::RAW_REPORT);
  }

  public function it_records_time_of_construction()
  {
    $this->getTime()->shouldHaveType(\DateTime::class);
  }
}
