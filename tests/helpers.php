<?php

use PHPUnit\Framework\TestCase;

class HelpersTest extends TestCase
{
  /**
   * @test
   */
  public function sort_using_keys_order()
  {
    $map = ['c' => 3, 'a' => 1, 'b' => 2];
    $keysOrder = ['a', 'b', 'c'];

    uksort($map, helpers\sort_by_keys($keysOrder));

    $this->assertEquals(['a', 'b', 'c'], array_keys($map));
    $this->assertEquals([1, 2, 3], array_values($map));
  }
}
