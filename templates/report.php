<style>
  table { border-collapse: collapse; }
  table, th, td { border: 1px solid black; padding: 10px }
  tbody tr:first-child { color: green }
</style>
<?php $benchmarkTestTitles = $benchmarkReport->getBenchmarkTitles(); ?>
<h1>Benchmark Report</h1>
<table>
  <thead>
    <tr>
      <th></th>
      <?php
        foreach($benchmarkTestTitles as $benchmarkTestTitle) {
          echo sprintf('<th colspan="2">%s</th>', $benchmarkTestTitle);
        }
      ?>
    </tr>
    <tr>
      <th>URL</th>
      <?php
        for ($i = 0; $i < count($benchmarkTestTitles); $i++) {
          echo '<th>SCORE</th><th>COMPARISION</th>';
        }
      ?>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($benchmarkReport->getResultsGroupedByURI() as $uri => $resultsGroupedByBenchmarkTest) : ?>
    <tr>
      <td><?php echo $uri; ?></td>
      <?php
        foreach ($resultsGroupedByBenchmarkTest as  $resultsForBenchmarkTest) {
          echo sprintf('<td>%s</td><td>%s</td>', $resultsForBenchmarkTest['score']['formatted'], $resultsForBenchmarkTest['comparision']['formatted']);
        }
      ?>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
